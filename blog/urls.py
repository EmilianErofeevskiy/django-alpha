
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('blog/', views.blog, name="blog"),
    path('search/', views.search_results, name="search_results"),
    path('post/<str:slug>/', views.post_detail, name="post_detail_url"),
    path('post/<str:slug>/leave_comment/', views.leave_comment, name="leave_comment"),
    path('category/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('register/', views.register, name="register"),
    path('contact/', views.contact, name="contact"),
    path('popular/', views.popular, name="popular"),
]
